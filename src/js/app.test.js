import '@testing-library/jest-dom/extend-expect';

test("Random test", () => {
  document.body.innerHTML = `
  <header>
  <h3 id="title">Lantern Tool</h3>
</header>
  `;

  const title = document.getElementById("title");

  expect(title).toBeInTheDocument();
});
