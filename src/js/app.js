// Modal Open and Close
var modalBox = document.getElementById("TweetModal");
var openModal = document.querySelector("#openModal");
var closeModal = document.querySelector("svg.close");

openModal.onclick = function(){
    modalBox.style.display = "grid";

    closeModal.onclick = function(){
        modalBox.style.display = "none";
    }
        window.onclick = function(event) {
        if (event.target == modalBox) {
            modalBox.style.display = "none";
        }
    }
}

        
// Logout Open and Close
function openLogout(){
    var logoutBox = document.getElementById("LogoutModal");
        logoutBox.style.display = "grid";

    window.onclick = function(event) {
        if (event.target == logoutBox) {
            logoutBox.style.display = "none";
        }
    }
}


//Tweet Button Active
var tweetInput = document.querySelector('.modalContent form input');
var btnTweet = document.querySelector('.modalContent form button');

tweetInput.oninput = function(){
    btnTweet.removeAttribute('disabled');
    btnTweet.style.backgroundColor = 'rgba(29,161, 242, 1)';
}

var whatsUpInput = document.querySelector('#whatsUp form input');
var whatsUpTweet = document.querySelector('#whatsUp form button');

whatsUpInput.oninput = function(){
    whatsUpTweet.removeAttribute('disabled');
    whatsUpTweet.style.backgroundColor = 'rgba(29,161, 242, 1)';
}


// Iput Empty Valu
function emptyMainValue(){
    var Inputs = document.querySelector(".modalContent form.create input");
        whatsUpTweet.setAttribute('disabled', true);
        whatsUpTweet.style.backgroundColor = 'rgba(29,161, 242, .5)';
}
function emptyModalValue(){
    var Inputs = document.querySelector(".modalContent form.create input");
    modalBox.style.display = "none";
    btnTweet.setAttribute('disabled', true);
    btnTweet.style.backgroundColor = 'rgba(29,161, 242, .5)';
}


// Psge Change
var Home = document.getElementById("home");
var Profile = document.getElementById("profile");
var btnHome = document.querySelector("#btnhome");
var btnProfile = document.querySelector("#btnprofile");
var rightSide = document.querySelector("#sheredRightSide");
var trends = document.querySelector("#trends");
var follow = document.querySelector("#WhoToFollow");

btnHome.onclick = function(){
    Home.style.display = "grid";
    btnHome.classList.add("active");
    btnHome.firstElementChild.classList.add("active");
    Profile.style.display = "none";
    btnProfile.classList.remove("active");
    btnProfile.firstElementChild.classList.remove("active");
    rightSide.style.gridTemplateRows = "1fr 14fr 8fr 1fr";
    trends.style.gridRow = "2";
    follow.style.gridRow = "3";
}

btnProfile.onclick = function(){
    Profile.style.display = "grid";
    btnProfile.classList.add("active");
    btnProfile.firstElementChild.classList.add("active");
    Home.style.display = "none";
    btnHome.classList.remove("active");
    btnHome.firstElementChild.classList.remove("active");
    rightSide.style.gridTemplateRows = "1fr 8fr 14fr 1fr";
    trends.style.gridRow = "3";
    follow.style.gridRow = "2";
}


// Serch Icone Blue
var serchField = document.getElementById("serchBar").firstElementChild;
var serchIcon = document.getElementById("serchBar").lastElementChild;

serchField.onfocus = function(){
    serchIcon.style.fill = "rgba(29,161, 242, 1)";
}

serchField.addEventListener('focusout', (event) => {
    serchIcon.style.fill = "rgba(0, 0, 0, 0.5)";    
  });
