// Modal Open and Close
var signInBox = document.getElementById("sign-inModal");
var openModal = document.querySelector("#popModal");

openModal.onclick = function(){
    signInBox.style.display = "grid";
}

window.onclick = function(event) {
    if (event.target == signInBox) {
        signInBox.style.display = "none";
    }
}


// Focuse User Feedback on Signup
var aSignInput = document.querySelectorAll(".sign-inContent form input");
var aSignLabel = document.querySelectorAll(".sign-inContent form label");

for (let i = 0; i < aSignInput.length; i++) {
    const element = aSignInput[i];
    aSignInput[i].onfocus = function(){
        aSignLabel[i].style.color = "rgba(29,161, 242, 1)";
    }  
    aSignInput[i].addEventListener('focusout', (event) => {
        aSignLabel[i].style.color = "rgba(0, 0, 0, 0.7)";    
    })   
}


// Signup Button Active
var lastInput = document.querySelector('.sign-inContent form input:last-of-type');
var btnSign = document.querySelector('.sign-inContent form button');

lastInput.oninput = function(){
    btnSign.removeAttribute('disabled');
    btnSign.style.backgroundColor = 'rgba(29,161, 242, 1)';
}


// Focuse User Feedback on Login
var aLoginInput = document.querySelectorAll(".log-inContent form input");
var aLoginLabel = document.querySelectorAll(".log-inContent form label");

for (let i = 0; i < aLoginInput.length; i++) {
    const element = aLoginInput[i];
    aLoginInput[i].onfocus = function(){
        aLoginLabel[i].style.color = "rgba(29,161, 242, 1)";
    }  
    aLoginInput[i].addEventListener('focusout', (event) => {
        aLoginLabel[i].style.color = "rgba(0, 0, 0, 0.7)";    
    })   
}


// login Button Active
var passInput = document.querySelector('.log-inContent form input:last-of-type');
var btnLogin = document.querySelector('.log-inContent form button');

passInput.oninput = function(){
    btnLogin.removeAttribute('disabled');
    btnLogin.style.backgroundColor = 'rgba(29,161, 242, 1)';
}
